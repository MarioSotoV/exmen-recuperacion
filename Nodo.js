class Nodo{
    constructor(nombre,valor,nivel,padre,Hl,HR) {
        this.nombre=nombre;
        this.valor=valor;
        this.nivel=nivel;
        this.padre=padre;
        this.HL=Hl;
        this.HR=HR;



        if(Hl==5)
            this.HL=this.crearHijo(5,5,nivel+1,padre+1,1,7);

        if(HR==78)
            this.HR=this.crearHijo(78,78,nivel+1,padre+1,45,90);

        if(Hl==1)
            this.HL=this.crearHijo(1,1,nivel+1,padre+1,0,4);
        if(HR==7)
            this.HR=this.crearHijo(7,7,nivel+1,padre+1,6,15);

        if(Hl==45)
            this.HL=this.crearHijo(45,45,nivel+1,padre+1,0,77);
        if(HR==90)
            this.HR=this.crearHijo(90,90,nivel+1,padre+1,80,99);

        if(HR==4)
            this.HR=this.crearHijo(4,4,nivel+1,padre+1,0,0);

        if(Hl==6)
            this.HL=this.crearHijo(6,6,nivel+1,padre+1,0,0);
        if(HR==15)
            this.HR=this.crearHijo(15,15,nivel+1,padre+1,0,0);


        if(HR==77)
            this.HR=this.crearHijo(77,77,nivel+1,padre+1,0,0);

        if(Hl==80)
            this.HL=this.crearHijo(80,80,nivel+1,padre+1,0,0);
        if(HR==99)
            this.HR=this.crearHijo(99,99,nivel+1,padre+1,0,0);

    }

    crearHijo(nombre,valor,nivel,padre,HL,HR){
        var crearHijo=new Nodo(nombre,valor,nivel,padre,HL,HR);
        return crearHijo;
    }
}